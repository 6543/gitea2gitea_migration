package main

import (
	"code.gitea.io/sdk/gitea"
	"fmt"
	"strings"
	"time"
)

func printError(err error) {
	fmt.Printf("Erorr: %v\n", err)
}

var (
	newOwner = "opyale"
	newRepo  = "GitNex2"

	oldOwner = "gitnex"
	oldRepo  = "GitNex"

	stringClosed = string(gitea.StateClosed)

	IssueSleep   = time.Second * 40
	CommentSleep = time.Second * 20
)

func createDummy(c *gitea.Client, index int64) {
	c.CreateIssue(newOwner, newRepo, gitea.CreateIssueOption{
		Title:  fmt.Sprintf("dummy %d", index),
		Body:   "",
		Closed: true,
	})
	time.Sleep(IssueSleep)
}

func createLableMap(oldC, newC *gitea.Client) map[int64]int64 {
	labelMap := make(map[int64]int64, 10)
	labelsOld, err := oldC.ListRepoLabels(oldOwner, oldRepo, gitea.ListLabelsOptions{})
	if err != nil {
		return labelMap
	}
	labelsNew, err := newC.ListRepoLabels(newOwner, newRepo, gitea.ListLabelsOptions{})
	if err != nil {
		return labelMap
	}
	for _, old := range labelsOld {
		for _, new := range labelsNew {
			if strings.TrimSpace(old.Name) == new.Name {
				labelMap[old.ID] = new.ID
			}
		}
		if labelMap[old.ID] == 0 {
			fmt.Println("Create Label: '" + old.Name + "'")
			label, err := newC.CreateLabel(newOwner, newRepo, gitea.CreateLabelOption{
				Name:        strings.TrimSpace(old.Name),
				Color:       "#" + old.Color,
				Description: old.Description,
			})
			if err != nil {
				printError(err)
				return labelMap
			}
			labelMap[old.ID] = label.ID
		}
	}
	return labelMap
}

func createMileMap(oldC, newC *gitea.Client) map[int64]int64 {
	mileMap := make(map[int64]int64, 10)
	milesOld, err := oldC.ListRepoMilestones(oldOwner, oldRepo, gitea.ListMilestoneOption{State: gitea.StateAll})
	if err != nil {
		return mileMap
	}
	milesNew, err := newC.ListRepoMilestones(newOwner, newRepo, gitea.ListMilestoneOption{State: gitea.StateAll})
	if err != nil {
		return mileMap
	}
	for _, old := range milesOld {
		for _, new := range milesNew {
			if old.Title == new.Title {
				mileMap[old.ID] = new.ID
			}
		}
		if mileMap[old.ID] == 0 {
			fmt.Println("Create Milestone: '" + old.Title + "'")
			mile, err := newC.CreateMilestone(newOwner, newRepo, gitea.CreateMilestoneOption{
				Title:       strings.TrimSpace(old.Title),
				Description: old.Description,
				Deadline:    old.Deadline,
			})
			if err != nil {
				printError(err)
				return mileMap
			}
			if old.State == gitea.StateClosed {
				mile, err = newC.EditMilestone(newOwner, newRepo, mile.ID, gitea.EditMilestoneOption{
					State: &stringClosed,
				})
				if err != nil {
					return mileMap
				}
			}
			mileMap[old.ID] = mile.ID
		}
	}
	return mileMap
}

func migComments(oldC *gitea.Client, newC map[string]*gitea.Client, index int64) error {
	comments, err := oldC.ListIssueComments(oldOwner, oldRepo, index, gitea.ListIssueCommentOptions{})
	if err != nil {
		return err
	}

	var body string
	for i := 0; i < len(comments); i += 1 {
		body = comments[i].Body
		if len(body) == 0 {
			body = "original is empty: " + comments[i].HTMLURL
		}
		if newC[comments[i].Poster.UserName] == nil {
			body = fmt.Sprintf("Created By: @%s\n\n"+body, comments[i].Poster.UserName)
			_, err = newC["6543"].CreateIssueComment(newOwner, newRepo, index, gitea.CreateIssueCommentOption{Body: body})
		} else {
			_, err = newC[comments[i].Poster.UserName].CreateIssueComment(newOwner, newRepo, index, gitea.CreateIssueCommentOption{Body: body})
		}

		if err != nil {
			return err
		}
		fmt.Printf("Comment [%d] created\n", comments[i].ID)
		time.Sleep(CommentSleep)
	}
	return nil
}

func main() {
	oldClient := gitea.NewClient("https://gitea.com", "token")
	newClients := make(map[string]*gitea.Client, 2)
	newClients["6543"] = gitea.NewClient("https://codeberg.org", "token")
	newClients["opyale"] = gitea.NewClient("https://codeberg.org", "token")
	newClients["mmarif"] = gitea.NewClient("https://codeberg.org", "token")

	lastIssue, err := oldClient.ListRepoIssues(oldOwner, oldRepo, gitea.ListIssueOption{ListOptions: gitea.ListOptions{Page: 1, PageSize: 1}, State: gitea.StateAll})
	if err != nil {
		printError(err)
		return
	}
	lastPull, err := oldClient.ListRepoPullRequests(oldOwner, oldRepo, gitea.ListPullRequestsOptions{ListOptions: gitea.ListOptions{Page: 1, PageSize: 1}, State: gitea.StateAll})
	if err != nil {
		printError(err)
		return
	}
	max_index := lastIssue[0].Index
	fmt.Printf("Max Index: %d\n\n", max_index)
	if lastPull[0].Index > max_index {
		max_index = lastPull[0].Index
	}

	labelMap := createLableMap(oldClient, newClients["6543"])
	mileMap := createMileMap(oldClient, newClients["6543"])

	var body string
	var title string
	var isClosed bool
	var labels []int64
	var mile int64

	for i := int64(1); i <= max_index; i = i + 1 {
		fmt.Printf("Do #%d: ", i)
		issue, err := oldClient.GetIssue(oldOwner, oldRepo, i)
		if err != nil {
			printError(err)
			createDummy(newClients["6543"], i)
		} else {

			body = issue.Body
			title = issue.Title
			labels = nil
			for _, l := range issue.Labels {
				if labelMap[l.ID] != 0 {
					labels = append(labels, labelMap[l.ID])
				}
			}
			mile = 0
			if issue.Milestone != nil {
				mile = mileMap[issue.Milestone.ID]
			}

			if newClients[issue.Poster.UserName] == nil {
				body = fmt.Sprintf("Created By: @%s\n\n"+body, issue.Poster.UserName)
			}

			if pull, err := oldClient.GetPullRequest(oldOwner, oldRepo, i); err == nil {
				// is pull
				fmt.Printf("isPull...\n")

				if pull.State == gitea.StateOpen {
					createPullOpts := gitea.CreatePullRequestOption{
						Head:     fmt.Sprintf("pull_%d", i),
						Base:     pull.Base.Name,
						Title:    pull.Title,
						Body:     body,
						Deadline: pull.Deadline,
					}
					if newClients[issue.Poster.UserName] == nil {
						_, err = newClients["6543"].CreatePullRequest(newOwner, newRepo, createPullOpts)
					} else {
						_, err = newClients[issue.Poster.UserName].CreatePullRequest(newOwner, newRepo, createPullOpts)
					}
					if err == nil {
						var assignees []string
						for _, a := range issue.Assignees {
							if b, err := newClients["6543"].IsCollaborator(newOwner, newRepo, a.UserName); err == nil && b {
								assignees = append(assignees, a.UserName)
							}
						}
						if len(assignees) != 0 {
							_, _ = newClients["6543"].EditIssue(newOwner, newRepo, i, gitea.EditIssueOption{
								Assignees: assignees,
							})
						}

						// migriere comments
						if issue.Comments != 0 {
							if err = migComments(oldClient, newClients, i); err != nil {
								printError(err)
								return
							}
						}
						time.Sleep(IssueSleep)
						continue
					}
				}

				title = "[Pull] " + title
				body = body + "\n\n---\nOriginal Pull att :" + pull.HTMLURL
				if pull.MergedCommitID != nil && len(*pull.MergedCommitID) != 0 {
					body = body + "\n\nMerged with: " + *pull.MergedCommitID
				}
			} else {
				// is issue
				fmt.Printf("isIssue...\n")
			}

			if issue.State == gitea.StateClosed {
				isClosed = true
			} else {
				isClosed = false
			}

			createIssueOpts := gitea.CreateIssueOption{
				Title:     title,
				Body:      body,
				Closed:    isClosed,
				Labels:    labels,
				Milestone: mile,
			}

			var new *gitea.Issue
			if newClients[issue.Poster.UserName] == nil {
				new, err = newClients["6543"].CreateIssue(newOwner, newRepo, createIssueOpts)
			} else {
				new, err = newClients[issue.Poster.UserName].CreateIssue(newOwner, newRepo, createIssueOpts)
			}
			time.Sleep(IssueSleep)
			if err != nil {
				printError(err)
				if new, err = newClients["6543"].GetIssue(newOwner, newRepo, i); err != nil || new == nil {
					fmt.Printf("can not create issue with index %d", i)
					return
				}
				continue
			}

			if new.Index != i {
				fmt.Println("Error: new.Index != i")
				return
			}

			var assignees []string
			for _, a := range issue.Assignees {
				if b, err := newClients["6543"].IsCollaborator(newOwner, newRepo, a.UserName); err == nil && b {
					assignees = append(assignees, a.UserName)
				}
			}
			if len(assignees) != 0 {
				_, _ = newClients["6543"].EditIssue(newOwner, newRepo, i, gitea.EditIssueOption{
					Assignees: assignees,
				})
			}

			// migriere comments
			if issue.Comments != 0 {
				if err = migComments(oldClient, newClients, i); err != nil {
					printError(err)
					return
				}
			}

			fmt.Printf("...Done\n")
		}

	}
}
